Інструкції для командного рядка
Також ви можете завантажувати файли з вашого комп'ютера за допомогою нижченаведених інструкцій.


Глобальні налаштування Git
git config --global user.name "Kornych Lyceum"
git config --global user.email "kornych@bigmir.net"

Створити новий репозиторій
git clone https://gitlab.com/informatyka1/8_klas_1_group/python.git
cd python
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Відправити існуючу папку
cd existing_folder
git init
git remote add origin https://gitlab.com/informatyka1/8_klas_1_group/python.git
git add .
git commit -m "Initial commit"
git push -u origin master

Відправити існуючий репозиторій Git
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/informatyka1/8_klas_1_group/python.git
git push -u origin --all
git push -u origin --tags